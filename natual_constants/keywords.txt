##################################
# Syntax Coloring Map For Library
# Datatypes (KEYWORD1) bold + orange
# Methods and Functions (KEYWORD2) 
# Instances (KEYWORD2) orange
# Constants (LITERAL1)  blue
# Variables (LITERAL2)  blue
#  you may adjust the setting by editing [IDE-directory]/lib/theme/theme.txt
##################################

natural_constants	KEYWORD1
NC_c_lightspeed	LITERAL1
NC_h_planck	LITERAL1
NC_e_charge	LITERAL1
NC_k_boltzmann	LITERAL1
NC_G_gravitation	LITERAL1
NC_T_null_kelvin	LITERAL1
NC_N_avogadro	LITERAL1
NC_R_universal	LITERAL1
NC_molar_mass_dry_air	LITERAL1
NC_molar_mass_water	LITERAL1
NC_molar_mass_nitrogen	LITERAL1
NC_molar_mass_oxygen	LITERAL1
NC_Rs_dry_air	LITERAL1
NC_Rs_water	LITERAL1
NC_Rs_nitrogen	LITERAL1
NC_Rs_oxygen	LITERAL1
NC_my0	LITERAL1
NC_g_acceleration	LITERAL1
NC_Z_w0	LITERAL1
NC_m_elektron	LITERAL1
NC_pi	LITERAL1
value	LITERAL2
uncertainty	LITERAL2
