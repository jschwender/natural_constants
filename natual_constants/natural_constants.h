/*
  Library with natural constants and related uncertainty. 
  Values that may be necessary to calculate measurement values into other values of interest
*/
typedef struct { double value, uncertainty; } nc;
#define fixed 0
const nc         NC_c_lightspeed = { 299792458,       fixed };    // fundamental constant in m/s
const nc             NC_h_planck = { 6.62607015e-34,  fixed };    // fundamental constant in J s
const nc             NC_e_charge = { 1.602176634e-19, fixed };    // fundamental constant in C
const nc          NC_k_boltzmann = { 1.380649e-23,    fixed };    // fundamental constant Boltzmann-constant in J/K
const nc        NC_G_gravitation = { 6.67430e-11,     fixed };    // fundamental constant gravitation constant

const nc        NC_T_null_kelvin = { -273.15,         fixed };    // 
const nc           NC_N_avogadro = { 6.02214076e23,   fixed };    // Avogadro-Konstante in 1/mol
const nc          NC_R_universal = { NC_N_avogadro.value * NC_k_boltzmann.value, fixed };   // 8.31446261815324 Universal Gas Constant in J /mol/K 

const nc   NC_molar_mass_dry_air = { 0.0289644, 0.000001 };   // standard atmosphere definition of dry air in kg/mol
const nc     NC_molar_mass_water = { 0.01802, 0.000001 };     // in kg/mol
const nc  NC_molar_mass_nitrogen = { 0.0140067, 0.000425 };   // in kg/mol
const nc    NC_molar_mass_oxygen = { 0.015999, 0.00037 };     // in kg/mol

const nc           NC_Rs_dry_air = { NC_R_universal.value / NC_molar_mass_dry_air.value, 
                                     NC_R_universal.value / NC_molar_mass_dry_air.value * NC_molar_mass_dry_air.uncertainty };  // 287 J/kg/k
const nc             NC_Rs_water = { NC_R_universal.value / NC_molar_mass_water.value,
                                     NC_R_universal.value / NC_molar_mass_water.value * NC_molar_mass_water.uncertainty };   // 461,  J/kg/K
const nc          NC_Rs_nitrogen = { NC_R_universal.value / NC_molar_mass_nitrogen.value,
                                     NC_R_universal.value / NC_molar_mass_nitrogen.value * NC_molar_mass_nitrogen.uncertainty };   // in  J/kg/K
const nc            NC_Rs_oxygen = { NC_R_universal.value / NC_molar_mass_oxygen.value,
                                     NC_R_universal.value / NC_molar_mass_oxygen.value * NC_molar_mass_oxygen.uncertainty };   // in  J/kg/K

const nc                  NC_my0 = { 1.25663706212e-6, 1e-18 }; // N / A², or 4×PI×1E-7
const nc       NC_g_acceleration = { 9.80665,          0.005 }; // standard value and it's variarion depending on location, in m/s²
const nc                 NC_Z_w0 = { 3.76730313667e2,  1e-18 }; // wave resistance of the vacuums in Ohm (my0 × c)
const nc           NC_m_elektron = { 9.1093837015e-31, 1e-44 }; // electron mass in kg

// PI is already provided by math.h
const nc                   NC_pi = { 3.141592653589793238462, 1e-16 };  // uncertainty limited by number format double
